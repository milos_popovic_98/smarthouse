/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package musicplayer;

import is1.commands.Command;
import is1.visitors.VisitorImpl;
import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Topic;

/**
 *
 * @author popov
 */
public class Main {
    private static int MUSIC_PLAYER = 1;
    
    @Resource(lookup = "jms/__defaultConnectionFactory")
    private static ConnectionFactory connectionFactory;

    @Resource(lookup = "IsTopic")
    private static Topic topic;

    @Resource(lookup = "IsQueue")
    private static Queue queue;
    
    public static void main(String[] args) {
        System.out.println("Pokrenut " + MUSIC_PLAYER);
        JMSContext context = connectionFactory.createContext();
        context.setClientID("Id" + MUSIC_PLAYER);
        
        JMSConsumer consumer = context.createDurableConsumer(topic, "sub", "Id=" + MUSIC_PLAYER, false);
        JMSProducer producer = context.createProducer();

        while (true) {
            try {
                ObjectMessage message = (ObjectMessage) consumer.receive();
                Command command = (Command) message.getObject();
                System.out.println("Primljena komanda ");
                command.execute(new VisitorImpl());
                //System.out.println(command.getSenderId() + " " + (String)command.getResponse());
                ObjectMessage response = context.createObjectMessage(command);
                producer.send(queue, response);
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }
    
}
