package is1;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.awt.*;
import java.io.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PlayerService {

    private static String URL_ADDRESS = "https://deezerdevs-deezer.p.rapidapi.com/search?q=";
    private static String API_KEY = "d96adba8b5msh36cd81079239aa2p1effb8jsn80274830987a";
    private static String HOST = "deezerdevs-deezer.p.rapidapi.com";

    private static PlayerService service;

    private PlayerService() {
    }

    public static PlayerService getService() {
        if (service == null) {
            service = new PlayerService();
        }
        return service;
    }

    public String searchSong(String songName) throws IOException, InterruptedException, JSONException {
        String encodedSongName = songName.replaceAll(" ", "%20");
        String command = "curl -X GET "
                + " -H \"x-rapidapi-key: " + API_KEY
                + "\" -H \"x-rapidapi-host: " + HOST + "\" "
                + URL_ADDRESS + encodedSongName;

        ProcessBuilder processBuilder = new ProcessBuilder(command.split(" "));
        processBuilder.directory(new File("C:/Users/popov/"));
        Process process = processBuilder.start();
        InputStream in = process.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
        JSONTokener tokener = new JSONTokener(bufferedReader);
        JSONObject responseJSON = new JSONObject(tokener);
        JSONArray data = responseJSON.getJSONArray("data");
        if (!data.isEmpty()) {
            JSONObject data0 = data.getJSONObject(0);
            String title = data0.getString("title");
            return data0.getString("link");
        } else {
            return null;
        }
    }

    public void openSongInBrowser(URI uri, String link) throws IOException {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            desktop.browse(uri);
        } else {
            String command = "cmd.exe /c  start " + link;
            System.out.println(command);
            ProcessBuilder processBuilder = new ProcessBuilder(command.split(" "));
            processBuilder.directory(new File("C:/Users/popov/"));
            processBuilder.start();
        }

    }
}
