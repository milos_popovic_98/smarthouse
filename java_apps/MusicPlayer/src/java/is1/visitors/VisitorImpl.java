/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is1.visitors;

import is1.PlayerService;
import is1.commands.CalculateDistance;
import is1.commands.GeocodeLocation;
import is1.commands.PlaySong;
import is1.commands.SetAlarm;
import is1.entities.Song;
import is1.entities.User;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.ws.rs.ext.Provider;
import org.json.JSONException;

/**
 *
 * @author popov
 */
public class VisitorImpl implements Visitor {
        
    public VisitorImpl(EntityManager em) {
        //this.em = em;
    }
    public VisitorImpl() {}
    
    @Override
    public void visit(PlaySong playSong) {
        try {
            System.out.println("Pozvan execute " + playSong.getSongName());
            PlayerService playerService = PlayerService.getService();
            String link = null;
            link = playerService.searchSong(playSong.getSongName());
            if (link == null) {
                playSong.setResponse("Nije pronadjena pesma zadatog naziva.");
                return;
            }
            URL url = new URL(link);
            playerService.openSongInBrowser(url.toURI(), link);
            playSong.setResponse("OK");
            return;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException ex) {
            Logger.getLogger(PlaySong.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(VisitorImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        playSong.setResponse("Nije pronadjena pesma zadatog naziva.");
    }

    @Override
    public void visit(GeocodeLocation geocodeLocation) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(CalculateDistance calculateDistance) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(SetAlarm setAlarm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    

}
