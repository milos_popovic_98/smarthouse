/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is1.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author popov
 */
@Entity
@Table(name = "location")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Location.findAll", query = "SELECT l FROM Location l"),
    @NamedQuery(name = "Location.findByLatitude", query = "SELECT l FROM Location l WHERE l.latitude = :latitude"),
    @NamedQuery(name = "Location.findByLongitude", query = "SELECT l FROM Location l WHERE l.longitude = :longitude"),
    @NamedQuery(name = "Location.findByIdLoc", query = "SELECT l FROM Location l WHERE l.idLoc = :idLoc")})
public class Location implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "latitude")
    private BigDecimal latitude;
    @Basic(optional = false)
    @NotNull
    @Column(name = "longitude")
    private BigDecimal longitude;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdLoc")
    private Integer idLoc;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLoc")
    private List<Eventlocation> eventlocationList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLoc")
    private List<Home> homeList;

    public Location() {
    }

    public Location(Integer idLoc) {
        this.idLoc = idLoc;
    }

    public Location(Integer idLoc, BigDecimal latitude, BigDecimal longitude) {
        this.idLoc = idLoc;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public Integer getIdLoc() {
        return idLoc;
    }

    public void setIdLoc(Integer idLoc) {
        this.idLoc = idLoc;
    }

    @XmlTransient
    public List<Eventlocation> getEventlocationList() {
        return eventlocationList;
    }

    public void setEventlocationList(List<Eventlocation> eventlocationList) {
        this.eventlocationList = eventlocationList;
    }

    @XmlTransient
    public List<Home> getHomeList() {
        return homeList;
    }

    public void setHomeList(List<Home> homeList) {
        this.homeList = homeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLoc != null ? idLoc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Location)) {
            return false;
        }
        Location other = (Location) object;
        if ((this.idLoc == null && other.idLoc != null) || (this.idLoc != null && !this.idLoc.equals(other.idLoc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "is1.enitities.Location[ idLoc=" + idLoc + " ]";
    }
    
}
