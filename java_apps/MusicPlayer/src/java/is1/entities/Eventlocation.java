/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is1.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author popov
 */
@Entity
@Table(name = "eventlocation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Eventlocation.findAll", query = "SELECT e FROM Eventlocation e"),
    @NamedQuery(name = "Eventlocation.findByIdEvent", query = "SELECT e FROM Eventlocation e WHERE e.idEvent = :idEvent")})
public class Eventlocation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdEvent")
    private Integer idEvent;
    @JoinColumn(name = "IdEvent", referencedColumnName = "IdEvent", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Event event;
    @JoinColumn(name = "IdLoc", referencedColumnName = "IdLoc")
    @ManyToOne(optional = false)
    private Location idLoc;

    public Eventlocation() {
    }

    public Eventlocation(Integer idEvent) {
        this.idEvent = idEvent;
    }

    public Integer getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(Integer idEvent) {
        this.idEvent = idEvent;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Location getIdLoc() {
        return idLoc;
    }

    public void setIdLoc(Location idLoc) {
        this.idLoc = idLoc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEvent != null ? idEvent.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Eventlocation)) {
            return false;
        }
        Eventlocation other = (Eventlocation) object;
        if ((this.idEvent == null && other.idEvent != null) || (this.idEvent != null && !this.idEvent.equals(other.idEvent))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "is1.enitities.Eventlocation[ idEvent=" + idEvent + " ]";
    }
    
}
