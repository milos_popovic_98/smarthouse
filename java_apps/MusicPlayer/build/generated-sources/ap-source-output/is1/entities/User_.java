package is1.entities;

import is1.entities.Event;
import is1.entities.Home;
import is1.entities.Song;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-07-08T09:50:53")
@StaticMetamodel(User.class)
public class User_ { 

    public static volatile SingularAttribute<User, Integer> idUser;
    public static volatile SingularAttribute<User, String> password;
    public static volatile ListAttribute<User, Event> eventList;
    public static volatile ListAttribute<User, Song> songList;
    public static volatile SingularAttribute<User, Integer> idUser1;
    public static volatile SingularAttribute<User, String> username;
    public static volatile SingularAttribute<User, Home> home;

}