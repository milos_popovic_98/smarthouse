package is1.entities;

import is1.entities.Event;
import is1.entities.Location;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-07-08T09:50:53")
@StaticMetamodel(Eventlocation.class)
public class Eventlocation_ { 

    public static volatile SingularAttribute<Eventlocation, Location> idLoc;
    public static volatile SingularAttribute<Eventlocation, Integer> idEvent;
    public static volatile SingularAttribute<Eventlocation, Event> event;

}