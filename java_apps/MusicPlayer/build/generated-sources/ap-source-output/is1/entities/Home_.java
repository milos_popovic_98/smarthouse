package is1.entities;

import is1.entities.Location;
import is1.entities.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-07-08T09:50:53")
@StaticMetamodel(Home.class)
public class Home_ { 

    public static volatile SingularAttribute<Home, Integer> idUser;
    public static volatile SingularAttribute<Home, Location> idLoc;
    public static volatile SingularAttribute<Home, User> user;

}