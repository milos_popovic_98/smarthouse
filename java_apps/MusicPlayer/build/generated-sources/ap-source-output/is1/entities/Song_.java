package is1.entities;

import is1.entities.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-07-08T09:50:53")
@StaticMetamodel(Song.class)
public class Song_ { 

    public static volatile SingularAttribute<Song, User> idUser;
    public static volatile SingularAttribute<Song, Integer> idSong;
    public static volatile SingularAttribute<Song, String> title;

}