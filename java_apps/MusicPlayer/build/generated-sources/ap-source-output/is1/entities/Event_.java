package is1.entities;

import is1.entities.Eventlocation;
import is1.entities.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-07-09T11:13:34")
@StaticMetamodel(Event.class)
public class Event_ { 

    public static volatile SingularAttribute<Event, User> idUser;
    public static volatile SingularAttribute<Event, Eventlocation> eventlocation;
    public static volatile SingularAttribute<Event, String> name;
    public static volatile SingularAttribute<Event, Integer> idEvent;
    public static volatile SingularAttribute<Event, Date> startTime;
    public static volatile SingularAttribute<Event, Date> endTime;

}