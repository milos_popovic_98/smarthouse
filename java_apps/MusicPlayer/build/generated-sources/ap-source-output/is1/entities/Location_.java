package is1.entities;

import is1.entities.Eventlocation;
import is1.entities.Home;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-07-08T09:50:53")
@StaticMetamodel(Location.class)
public class Location_ { 

    public static volatile SingularAttribute<Location, Integer> idLoc;
    public static volatile SingularAttribute<Location, BigDecimal> latitude;
    public static volatile ListAttribute<Location, Home> homeList;
    public static volatile ListAttribute<Location, Eventlocation> eventlocationList;
    public static volatile SingularAttribute<Location, BigDecimal> longitude;

}