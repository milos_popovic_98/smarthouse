package is1.userservice.resources;

import is1.Receiver;
import is1.Sender;
import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import javax.jms.Topic;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 *
 * @author 
 */
@Path("hello")
public class JavaEE8Resource {
    
    
    @GET
    public Response ping(){
        
        return Response
                .ok("ping")
                .build();
    }
}
