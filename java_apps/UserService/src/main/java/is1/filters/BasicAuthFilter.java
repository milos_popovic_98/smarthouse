package is1.filters;

import is1.entities.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.StringTokenizer;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.UriInfo;

@Provider
public class BasicAuthFilter implements ContainerRequestFilter {

    @PersistenceContext(unitName = "default")
    EntityManager em;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        List<String> authHeaderValues = requestContext.getHeaders().get("Authorization");

        if (authHeaderValues != null && authHeaderValues.size() > 0) {
            String authHeaderValue = authHeaderValues.get(0);
            String decodedAuthHeaderValue = new String(Base64.getDecoder().decode(authHeaderValue.replaceFirst("Basic ", "")), StandardCharsets.UTF_8);
            StringTokenizer stringTokenizer = new StringTokenizer(decodedAuthHeaderValue, ":");
            String username = stringTokenizer.nextToken();
            String password = stringTokenizer.nextToken();
            
            String method = requestContext.getMethod();
            UriInfo uriInfo = requestContext.getUriInfo();
            List<PathSegment> pathSegments = uriInfo.getPathSegments();
            String endpointName = pathSegments.get(0).getPath();
            if ("POST".equals(method) && "register".equals(endpointName)) return;
            
            List<User> users = em.createQuery("SELECT u FROM User u WHERE u.username=:username", User.class)
                    .setParameter("username", username)
                    .getResultList();

            if(users.size() != 1){
                Response response = Response.status(Response.Status.UNAUTHORIZED).entity("Korisnicko ime ili sifra nije ispravno.").build();
                requestContext.abortWith(response);
                return;
            }

            User user = users.get(0);

            if(!user.getPassword().equals(password)){
                Response response = Response.status(Response.Status.UNAUTHORIZED).entity("Korisnicko ime ili sifra nije ispravno.").build();
                requestContext.abortWith(response);
                return;
            }
            return;
        }
        requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
    }
}
