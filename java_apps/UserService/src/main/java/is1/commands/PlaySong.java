/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is1.commands;

import is1.visitors.Visitor;


/**
 *
 * @author popov
 */
public class PlaySong implements Command {
    private static final long SerialVersionUID = 10l;
    private static int MUSIC_PLAYER = 1;
    private String songName;
    private String response;
    private int senderId;
    private int userId;
    
    public PlaySong(String songName) {
        this.songName = songName;
    }

    @Override
    public int getSenderId() {
        return senderId;
    }

    @Override
    public void setSenderId(int id) {
        senderId = id;
    }

    @Override
    public int getReceiverId() {
        return MUSIC_PLAYER;
    }

    public String getResponse() {
        return response;
    }

    @Override
    public void execute(Visitor visitor) {
        visitor.visit(this);
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Override
    public void setUserid(int id) {
        userId = id;
    }

    @Override
    public int getUserId() {
      return userId;
    } 
}
