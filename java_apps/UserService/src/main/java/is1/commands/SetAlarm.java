/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is1.commands;

import is1.visitors.Visitor;
import java.util.Date;

/**
 *
 * @author popov
 */
public class SetAlarm implements Command{
    
    private static int ALARM = 3;
    private int senderId;
    private int userId;
    private Date timestamp;
    private boolean periodical;
    
    @Override
    public int getSenderId() {
        return senderId;
    }

    @Override
    public void setSenderId(int id) {
        senderId = id;
    }

    @Override
    public int getReceiverId() {
        return ALARM;
    }

    @Override
    public void execute(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void setUserid(int id) {
        userId = id;
    }

    @Override
    public int getUserId() {
        return userId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isPeriodical() {
        return periodical;
    }

    public void setPeriodical(boolean periodical) {
        this.periodical = periodical;
    }
    
    public String getResponse() {
        return "OK";
    }
}
