package is1;

import is1.commands.Command;

import javax.annotation.Resource;
import javax.jms.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Sender implements Runnable {

    private ConnectionFactory connectionFactory;

    private Topic topic;

    public Sender(ConnectionFactory connectionFactory, Topic topic) {
        this.connectionFactory = connectionFactory;
        this.topic = topic;
    }

    @Override
    public void run() {
        JMSContext context = connectionFactory.createContext();
        JMSProducer producer = context.createProducer();
        LinkedBlockingQueue<Command> commands = JMSScheduler.getScheduler().getSenderQueue();

        while (!Thread.interrupted()) {
            try {
                Command command = commands.take();
                System.out.println("Dohvacena komanda " + command.getReceiverId() + " " + command.getSenderId());
                ObjectMessage message = context.createObjectMessage(command);
                message.setIntProperty("Id", command.getReceiverId());
                producer.send(topic, message);
                System.out.println("Poslata komanda");
            } catch (JMSException e) {
                e.printStackTrace();
            } catch (InterruptedException ex) {
                Logger.getLogger(Sender.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
}
