package is1.endpoints;

import is1.JMSScheduler;
import is1.commands.CalculateDistance;
import is1.commands.GeocodeLocation;
import is1.entities.Event;
import is1.entities.Location;
import is1.entities.User;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.Semaphore;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

@Path("calculate")
@Stateless
public class CalculatorController {

    @PersistenceContext(unitName = "default")
    EntityManager em;

    @GET
    public Response calculateDistance(@QueryParam("start") String location1,
                                      @QueryParam("target") String location2,
                                      @Context HttpHeaders httpHeaders) {
        int userId = getUserId(httpHeaders);
        User user = em.find(User.class, userId);
        Location home = new Location(user.getLocationLon(), user.getLocationLat());
        System.out.println(home.getLocationLat() + " " + home.getLocationLon());
        sendCommand(userId, location1, home);
        Location start = receiveResponse();
        System.out.println(start.getLocationLat() + " " + start.getLocationLon());
        if (start == null) {
            return Response.status(Response.Status.OK).entity("Lokacija " + location1 + " nije pronadjena").build();
        }
        sendCommand(userId, location2, home);
        Location target = receiveResponse();
        if (target == null) {
            return Response.status(Response.Status.OK).entity("Lokacija " + location2 + " nije pronadjena").build();
        }
        System.out.println(target.getLocationLat() + " " + target.getLocationLon());
        CalculateDistance command = new CalculateDistance();
        command.setStart(start);
        command.setTarget(target);
        command.setUserid(userId);
        JMSScheduler scheduler = JMSScheduler.getScheduler();
        Semaphore semaphore = new Semaphore(0);
        scheduler.addCommandToQueue(command, semaphore);
        semaphore.acquireUninterruptibly();
        Long travelTimeInSeconds = ((CalculateDistance)scheduler.getReceivedObject()).getResponse();
        System.out.println(travelTimeInSeconds);
        scheduler.signal();
        return Response.status(Response.Status.OK).entity(travelTimeInSeconds).build();
    }

    @GET
    @Path("current")
    public Response calculateDistance(@QueryParam("target") String location, @Context HttpHeaders httpHeaders) {
        int userId = getUserId(httpHeaders);
        User user = em.find(User.class, userId);
        Location start = getLastEventOrHome(user);
        System.out.println(start.getLocationLat() + " " + start.getLocationLon());
        sendCommand(userId, location, start);
        Location target = receiveResponse();
        if (target == null) {
            return Response.status(Response.Status.OK).entity("Lokacija " + location + " nije pronadjena").build();
        }
        CalculateDistance command = new CalculateDistance();
        command.setStart(start);
        command.setTarget(target);
        command.setUserid(userId);
        System.out.println(command.getStart().getLocationLat() + " " + command.getStart().getLocationLon());
        JMSScheduler scheduler = JMSScheduler.getScheduler();
        Semaphore semaphore = new Semaphore(0);
        scheduler.addCommandToQueue(command, semaphore);
        semaphore.acquireUninterruptibly();
        Long travelTimeInSeconds = ((CalculateDistance)scheduler.getReceivedObject()).getResponse();
        scheduler.signal();
        return Response.status(Response.Status.OK).entity(travelTimeInSeconds).build();
    }
    
    protected int getUserId(HttpHeaders httpHeaders) {
        List<String> authHeaderValues = httpHeaders.getRequestHeader("Authorization");
        
        if(authHeaderValues != null && authHeaderValues.size() > 0){
            String authHeaderValue = authHeaderValues.get(0);
            String decodedAuthHeaderValue = new String(Base64.getDecoder().decode(authHeaderValue.replaceFirst("Basic ", "")),StandardCharsets.UTF_8);
            StringTokenizer stringTokenizer = new StringTokenizer(decodedAuthHeaderValue, ":");
            String username = stringTokenizer.nextToken();
            
            User user = em.createNamedQuery("User.findByUsername", User.class).setParameter("username", username).getSingleResult();
            
            return user.getIdUser();
        }   
        return -1;
    }

    protected void sendCommand(int userId, String locationName, Location home) {
        GeocodeLocation command = new GeocodeLocation();
        command.setHome(home);
        command.setLocationName(locationName);
        command.setUserid(userId);
        JMSScheduler scheduler = JMSScheduler.getScheduler();
        Semaphore semaphore = new Semaphore(0);
        scheduler.addCommandToQueue(command, semaphore);
        semaphore.acquireUninterruptibly();
    }

    protected Location receiveResponse() {
       JMSScheduler scheduler = JMSScheduler.getScheduler();
       Location location = ((GeocodeLocation)scheduler.getReceivedObject()).getResponse();
       scheduler.signal();
       System.out.println(location.getLocationLon());
       return location;
    }

    protected Location getLastEventOrHome(User user) {
       TypedQuery<Event> query = em.createQuery("SELECT e FROM Event e WHERE e.idUser=:user AND e.startTime < :time ORDER BY e.startTime DESC", Event.class)
                .setParameter("time", new Date(System.currentTimeMillis()))
                .setParameter("user", user);      
        List<Event> temp = query.getResultList();
        if (!temp.isEmpty()) {
            return new Location(temp.get(0).getLocationLon(), temp.get(0).getLocationLat());
        } else {
            return new Location(user.getLocationLon(), user.getLocationLat());
        }
    }
}
