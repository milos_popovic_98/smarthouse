package is1.endpoints;

import is1.JMSScheduler;
import is1.commands.CalculateDistance;
import is1.commands.GeocodeLocation;
import is1.commands.SetAlarm;
import is1.entities.Event;
import is1.entities.Location;
import is1.entities.User;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.*;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;

@Path("events")
@Stateless
public class EventController {

    @PersistenceContext(unitName = "default")
    EntityManager em;

    @GET
    public Response getEvents(@Context HttpHeaders httpHeaders) {
        int id = getUserId(httpHeaders);
        User user = em.find(User.class, id);
        List<Event> eventList = em.createQuery("SELECT e FROM Event e WHERE e.idUser = :user", Event.class)
                .setParameter("user", user)
                .getResultList();
        List<String> ret = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        for (Event e : eventList) {
            ret.add(e.getIdEvent().toString());
            ret.add(e.getName());
            ret.add(sdf.format(e.getStartTime()));
            ret.add(sdf.format(e.getEndTime()));
        }
        return Response.status(Response.Status.OK).entity(new GenericEntity<List<String>>(ret) {
        }).build();
    }

    @POST
    @Path("add")
    public Response addEvent(@QueryParam("start1") String start1,
            @QueryParam("end1") String end1,
            @QueryParam("destination") String destination,
            @QueryParam("name") String name,
            @QueryParam("reminder") boolean flag,
            @Context HttpHeaders httpHeaders) {
        try {
            int userId = getUserId(httpHeaders);
            User user = em.find(User.class, userId);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date startTime = format.parse(start1);
            Date endTime = format.parse(end1);
            Location start = getLastEventOrHome(user, startTime);
            System.out.println("Start location: " + start.getLocationLat() + start.getLocationLon());
            Location target;
            System.out.println(destination);
            if ("".equals(destination)) {
                System.out.println("User location: " + user.getLocationLat() + " " + user.getLocationLon());
                target = new Location(user.getLocationLon(), user.getLocationLat());
            } else {
                sendCommand(userId, destination, start);
                target = receiveResponse();
                if (target == null) {
                    return Response.status(Response.Status.OK).entity("Lokacija " + destination + " nije pronadjena").build();
                }
            }
            CalculateDistance command = new CalculateDistance();
            command.setStart(start);
            command.setTarget(target);
            command.setUserid(userId);
            JMSScheduler scheduler = JMSScheduler.getScheduler();
            Semaphore semaphore = new Semaphore(0);
            scheduler.addCommandToQueue(command, semaphore);
            semaphore.acquireUninterruptibly();
            Long travelTimeInSeconds = ((CalculateDistance) scheduler.getReceivedObject()).getResponse();
            scheduler.signal();
            if (checkTimeConstraints(startTime, endTime, travelTimeInSeconds, 0, user)) {
                Event event = new Event();
                event.setIdUser(user);
                event.setName(name);
                event.setStartTime(startTime);
                event.setEndTime(endTime);
                event.setLocationLat(target.getLocationLat());
                event.setLocationLon(target.getLocationLon());
                em.persist(event);
                if (flag) {
                    Date timestamp = new Date(startTime.getTime() - travelTimeInSeconds * 1000);
                    SetAlarm command2 = new SetAlarm();
                    command2.setTimestamp(timestamp);
                    Semaphore semaphore2 = new Semaphore(0);
                    scheduler.addCommandToQueue(command2, semaphore2);
                    semaphore2.acquireUninterruptibly();
                    scheduler.signal();
                }
                return Response.status(Response.Status.OK).entity("Dogadjaj je uspesno dodat.").build();
            } else {
                return Response.status(Response.Status.OK).entity("Dogadjaj se preklapa sa postojecim obavezama.").build();
            }
        } catch (ParseException ex) {
            Logger.getLogger(EventController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.OK).entity("Datum nije u ispravnom formaatu.").build();
        }
    }

    @DELETE
    @Path("{idEvent}")
    public Response deleteEvent(@PathParam("idEvent") int idEvent, @Context HttpHeaders httpHeaders) {
        Event event = em.find(Event.class, idEvent);
        int id = getUserId(httpHeaders);
        if (event != null && event.getIdUser().getIdUser() == id) {
            em.remove(event);
        }
        return Response.status(Response.Status.OK).entity("OK").build();
    }

    @POST
    @Path("update/{idEvent}")
    public Response updateEvent(@PathParam("idEvent") int idEvent,
            @QueryParam("start1") String start1,
            @QueryParam("end1") String end1,
            @QueryParam("destination") String destination,
            @Context HttpHeaders httpHeaders) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date startTime = format.parse(start1);
            Date endTime = format.parse(end1);
            Event event = em.find(Event.class, idEvent);
            if (event != null) {
                int userId = getUserId(httpHeaders);
                User user = em.find(User.class, userId);
                Location start = getLastEventOrHome(user, startTime);
                Location target;
                if ("".equals(destination)) {
                    target = new Location(user.getLocationLon(), user.getLocationLat());
                } else {
                    sendCommand(userId, destination, start);
                    target = receiveResponse();
                    if (target == null) {
                        return Response.status(Response.Status.OK).entity("Lokacija " + destination + " nije pronadjena").build();
                    }
                }
                CalculateDistance command = new CalculateDistance();
                command.setStart(start);
                command.setTarget(target);
                command.setUserid(userId);
                JMSScheduler scheduler = JMSScheduler.getScheduler();
                Semaphore semaphore = new Semaphore(0);
                scheduler.addCommandToQueue(command, semaphore);
                semaphore.acquireUninterruptibly();
                Long travelTimeInSeconds = ((CalculateDistance) scheduler.getReceivedObject()).getResponse();
                scheduler.signal();
                if (checkTimeConstraints(startTime, endTime, travelTimeInSeconds, idEvent, user)) {
                    event.setIdUser(user);
                    event.setStartTime(startTime);
                    event.setEndTime(endTime);
                    event.setLocationLat(target.getLocationLat());
                    event.setLocationLon(target.getLocationLon());
                    return Response.status(Response.Status.OK).entity("Dogadjaj je uspesno dodat.").build();
                } else {
                    return Response.status(Response.Status.OK).entity("Dogadjaj se preklapa sa postojecim obavezama.").build();
                }
            } else {
                return Response.status(Response.Status.OK).entity("Dogadjaj ne postoji.").build();
            }
        } catch (ParseException ex) {
            Logger.getLogger(EventController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.OK).entity("Neispravan format datuma.").build();
        }
    }

    protected int getUserId(HttpHeaders httpHeaders) {
        List<String> authHeaderValues = httpHeaders.getRequestHeader("Authorization");

        if (authHeaderValues != null && authHeaderValues.size() > 0) {
            String authHeaderValue = authHeaderValues.get(0);
            String decodedAuthHeaderValue = new String(Base64.getDecoder().decode(authHeaderValue.replaceFirst("Basic ", "")), StandardCharsets.UTF_8);
            StringTokenizer stringTokenizer = new StringTokenizer(decodedAuthHeaderValue, ":");
            String username = stringTokenizer.nextToken();

            User user = em.createNamedQuery("User.findByUsername", User.class).setParameter("username", username).getSingleResult();

            return user.getIdUser();
        }
        return -1;
    }

    protected Location getLastEventOrHome(User user, Date startTime) {
        TypedQuery<Event> query = em.createQuery("SELECT e FROM Event e WHERE e.idUser=:user AND e.startTime < :time ORDER BY e.startTime DESC", Event.class)
                .setParameter("time", startTime)
                .setParameter("user", user);
        List<Event> temp = query.getResultList();
        if (!temp.isEmpty()) {
            return new Location(temp.get(0).getLocationLon(), temp.get(0).getLocationLat());
        } else {
            return new Location(user.getLocationLon(), user.getLocationLat());
        }
    }

    protected void sendCommand(int userId, String locationName, Location home) {
        GeocodeLocation command = new GeocodeLocation();
        command.setHome(home);
        command.setLocationName(locationName);
        command.setUserid(userId);
        JMSScheduler scheduler = JMSScheduler.getScheduler();
        Semaphore semaphore = new Semaphore(0);
        scheduler.addCommandToQueue(command, semaphore);
        semaphore.acquireUninterruptibly();
    }

    protected Location receiveResponse() {
        JMSScheduler scheduler = JMSScheduler.getScheduler();
        Location location = ((GeocodeLocation) scheduler.getReceivedObject()).getResponse();
        scheduler.signal();
        return location;
    }

    protected boolean checkTimeConstraints(Date startTime, Date endTime, Long travelTimeInSeconds, int eventId, User user) {
        TypedQuery<Event> query = em.createQuery("SELECT e FROM Event e WHERE e.idUser = :user AND e.idEvent != :eventId AND e.startTime < :time ORDER BY e.startTime DESC", Event.class)
                .setParameter("time", startTime)
                .setParameter("user", user)
                .setParameter("eventId", eventId)
                .setMaxResults(1);
        List<Event> temp = query.getResultList();
        if (!temp.isEmpty()) {
            if (temp.get(0).getEndTime().getTime() + travelTimeInSeconds * 1000 > startTime.getTime()) {
                return false;
            }
        }
        TypedQuery<Event> query2 = em.createQuery("SELECT e FROM Event e WHERE e.idUser = :user AND e.idEvent != :eventId AND e.startTime >= :time ORDER BY e.startTime", Event.class)
                .setParameter("time", startTime)
                .setParameter("user", user)
                .setParameter("eventId", eventId)
                .setMaxResults(1);
        temp = query2.getResultList();
        if (!temp.isEmpty()) {
            if (endTime.getTime() + travelTimeInSeconds * 1000 > temp.get(0).getStartTime().getTime()) {
                return false;
            }
        }
        return true;
    }
}
