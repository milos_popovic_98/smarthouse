package is1.endpoints;

import is1.JMSScheduler;
import is1.commands.Command;
import is1.commands.PlaySong;
import is1.entities.Song;
import is1.entities.User;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.StringTokenizer;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.concurrent.Semaphore;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;

@Path("music")
@Stateless
public class MusicController {
    
    @PersistenceContext(unitName = "default")
    EntityManager em;

    @GET
    public Response getSongs(@Context HttpHeaders httpHeaders) {
        int id = getUserId(httpHeaders);
        User user = em.find(User.class, id);
        List<String> songs = em.createQuery("SELECT s.title FROM Song s WHERE s.idUser = :user", String.class)
                .setParameter("user", user)
                .getResultList();
        return Response.status(Response.Status.OK).entity(new GenericEntity<List<String>>(songs){}).build();
    }

    @PUT
    public Response playSong(@QueryParam("song") String song, @Context HttpHeaders httpHeaders) {
        System.out.println("Pozvana komanda");
        Command command = new PlaySong(song);
        int id = getUserId(httpHeaders);
        command.setUserid(id);
        JMSScheduler scheduler = JMSScheduler.getScheduler();
        Semaphore semaphore = new Semaphore(0);
        scheduler.addCommandToQueue(command, semaphore);
        System.out.println("Cekanje na odgovor");
        semaphore.acquireUninterruptibly();
        System.out.println("Dobijen odgovor");
        String response = ((PlaySong)scheduler.getReceivedObject()).getResponse();
        System.out.println(response);
        scheduler.signal();
        if ("OK".equals(response)) {
            saveSongTitle(song, id);
        }
        return Response.status(Response.Status.OK).entity(response).build();
    }
    
    protected int getUserId(HttpHeaders httpHeaders) {
        List<String> authHeaderValues = httpHeaders.getRequestHeader("Authorization");
        
        if(authHeaderValues != null && authHeaderValues.size() > 0){
            String authHeaderValue = authHeaderValues.get(0);
            String decodedAuthHeaderValue = new String(Base64.getDecoder().decode(authHeaderValue.replaceFirst("Basic ", "")),StandardCharsets.UTF_8);
            StringTokenizer stringTokenizer = new StringTokenizer(decodedAuthHeaderValue, ":");
            String username = stringTokenizer.nextToken();
            
            User user = em.createNamedQuery("User.findByUsername", User.class).setParameter("username", username).getSingleResult();
            
            return user.getIdUser();
        }
        
        return -1;
    }
    
    protected void saveSongTitle(String title, int userId) {
        System.out.println("Cuvanje pesme");
        User user = em.find(User.class, userId);
        List<Song> songExists = em.createQuery("SELECT s FROM Song s WHERE s.idUser=:user AND s.title=:title", Song.class)
                .setParameter("user", user)
                .setParameter("title", title)
                .getResultList();
        if (songExists.isEmpty()) {
            System.out.println("Sacuvana pesma");
            Song song = new Song();
            song.setTitle(title);
            song.setIdUser(user);
            em.persist(song);
            em.flush();
        } else {
            System.out.println("Nije");
        }
    }
}
