/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is1.endpoints;

import is1.JMSScheduler;
import is1.Receiver;
import is1.Sender;
import is1.commands.GeocodeLocation;
import is1.entities.Location;
import is1.entities.User;
import java.math.BigDecimal;
import java.util.concurrent.Semaphore;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

/**
 *
 * @author popov
 */
@Path("register")
@Stateless
public class Controller {

    @PersistenceContext(unitName = "default")
    EntityManager em;

    @Resource(lookup = "jms/__defaultConnectionFactory")
    ConnectionFactory connectionFactory;

    @Resource(lookup = "IsTopic")
    private Topic topic;
    
    @Resource(lookup = "IsQueue")
    Queue queue;
    
    Thread sender, receiver;
    
    @POST
    public Response register(@QueryParam("username") String username,
            @QueryParam("password") String password,
            @QueryParam("location") String location) {
        try {
            if (sender == null || receiver == null) 
                startThreads();
            Location bias = new Location(new BigDecimal(20.457273), new BigDecimal(44.787197));
            GeocodeLocation command = new GeocodeLocation();
            command.setHome(bias);
            command.setLocationName(location);
            command.setUserid(0);
            JMSScheduler scheduler = JMSScheduler.getScheduler();
            Semaphore semaphore = new Semaphore(0);
            scheduler.addCommandToQueue(command, semaphore);
            semaphore.acquireUninterruptibly();
            Location response = ((GeocodeLocation) scheduler.getReceivedObject()).getResponse();
            scheduler.signal();
            System.out.println("Home: " + response.getLocationLat());
            User user = new User();
            user.setUsername(username);
            user.setPassword(password);
            user.setLocationLat(response.getLocationLat());
            user.setLocationLon(response.getLocationLon());
            em.persist(user);
            return Response.status(Response.Status.OK).entity("OK").build();
        } catch (ConstraintViolationException e) {
            e.printStackTrace();
        }
        return Response.status(Response.Status.OK).entity("Error").build();
    }
    
    @GET
    public Response login() {
        if (sender == null || receiver == null) 
            startThreads();
        return Response.status(Response.Status.OK).entity("OK").build();
    }
    
    protected void startThreads() {
        sender = new Thread(new Sender(connectionFactory, topic));
        receiver = new Thread(new Receiver(connectionFactory, queue));
        sender.start();
        receiver.start();
    }
}
