package is1.endpoints;

import is1.JMSScheduler;
import is1.commands.SetAlarm;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.ejb.Stateless;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("alarm1")
@Stateless
public class AlarmController {

    @POST
    public Response setAlarm(@QueryParam("time") String timestampStr,
                             @QueryParam("periodical") boolean periodical) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date timestamp = sdf.parse(timestampStr);
            SetAlarm command = new SetAlarm();
            command.setTimestamp(timestamp);
            command.setPeriodical(periodical);
            JMSScheduler scheduler = JMSScheduler.getScheduler();
            Semaphore semaphore = new Semaphore(0);
            scheduler.addCommandToQueue(command, semaphore);
            semaphore.acquireUninterruptibly();
            scheduler.signal();
            return Response.status(Response.Status.OK).entity("OK").build();
        } catch (ParseException ex) {
            Logger.getLogger(AlarmController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.OK).entity("Error").build();
        }
    }

    @POST
    @Path("melody")
    public Response configureMelody(@QueryParam("melody") int melody) {
        BufferedWriter writer = null;
        try {
            String fileName = "C:\\Users\\popov\\Desktop\\Informacioni sistemi\\is_projekat\\java_aplikacije\\Alarm3\\alarm.txt";
            writer = new BufferedWriter(new FileWriter(fileName));
            writer.write(melody);
            writer.close();
            return Response.status(Response.Status.OK).entity("OK").build();
        } catch (IOException ex) {
            Logger.getLogger(AlarmController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(AlarmController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return Response.status(Response.Status.OK).entity("Error").build();
        }
    }
}
