/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is1.visitors;

import is1.commands.PlaySong;
import is1.commands.GeocodeLocation;
import is1.commands.CalculateDistance;
import is1.commands.SetAlarm;
/**
 *
 * @author popov
 */
public interface Visitor {
    public void visit(PlaySong playSong);
    public void visit(GeocodeLocation geocodeLocation);
    public void visit(CalculateDistance calculateDistance);
    public void visit(SetAlarm setAlarm);
}
