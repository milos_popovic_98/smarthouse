/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is1.entities;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author popov
 */
public class Location implements Serializable{
    private BigDecimal locationLon;
    private BigDecimal locationLat;

    public Location(BigDecimal locationLon, BigDecimal locationLat) {
        this.locationLon = locationLon;
        this.locationLat = locationLat;
    }

    public BigDecimal getLocationLon() {
        return locationLon;
    }

    public void setLocationLon(BigDecimal locationLon) {
        this.locationLon = locationLon;
    }

    public BigDecimal getLocationLat() {
        return locationLat;
    }

    public void setLocationLat(BigDecimal locationLat) {
        this.locationLat = locationLat;
    }
    
    
}
