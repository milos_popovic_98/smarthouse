package is1;

import is1.commands.Command;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;

public class Receiver implements Runnable {

    private ConnectionFactory connectionFactory;
    private Queue queue;

    public Receiver(ConnectionFactory connectionFactory, Queue queue) {
        this.connectionFactory = connectionFactory;
        this.queue = queue;
    }

    
    @Override
    public void run() {
        JMSContext context = connectionFactory.createContext();
        JMSConsumer consumer = context.createConsumer(queue);
        JMSScheduler scheduler = JMSScheduler.getScheduler();

        while (!Thread.interrupted()) {
            try {
                ObjectMessage message = (ObjectMessage) consumer.receive();
                Command command = (Command) message.getObject();
                scheduler.receive(command);
            } catch (JMSException ex) {
                Logger.getLogger(Receiver.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
