package is1;


import is1.commands.Command;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

public class JMSScheduler {
    private static JMSScheduler scheduler;
    private static int LAST_ID;
    private LinkedBlockingQueue<Command> senderQueue;
    private Command receivedObject;
    private HashMap<Integer, Semaphore> blocked;
    private Semaphore syncSem;

    private JMSScheduler() {
        senderQueue = new LinkedBlockingQueue<>();
        blocked = new HashMap<>();
        syncSem = new Semaphore(1);
    }

    public static JMSScheduler getScheduler() {
        if (scheduler == null) {
            scheduler = new JMSScheduler();
        }
        return scheduler;
    }

    public LinkedBlockingQueue<Command> getSenderQueue() {
        return senderQueue;
    }
    synchronized public void addCommandToQueue(Command command, Semaphore semaphore) {
        LAST_ID = (LAST_ID + 1) % 100;
        blocked.put(LAST_ID, semaphore);
        command.setSenderId(LAST_ID);
        senderQueue.add(command);
        System.out.println("LAST_ID " + LAST_ID);
    }
    public void receive(Command command) {
        syncSem.acquireUninterruptibly();
        int id = command.getSenderId();
        System.out.println("Received " + id );
        receivedObject = command;
        Semaphore semaphore = blocked.remove(id);
        semaphore.release();
        System.out.println("Done receive");
    }

    public void signal() {
        syncSem.release();
        System.out.println("pozvan release");
    }

    public Object getReceivedObject() {
        return receivedObject;
    }
}
