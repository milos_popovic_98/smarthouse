/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is1.visitors;

import is1.commands.CalculateDistance;
import is1.commands.GeocodeLocation;
import is1.commands.PlaySong;
import is1.commands.SetAlarm;
import is1.timer.TimerQueue;
import java.util.Date;

/**
 *
 * @author popov
 */
public class VisitorImpl implements Visitor{

    @Override
    public void visit(PlaySong playSong) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(GeocodeLocation geocodeLocation) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(CalculateDistance calculateDistance) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(SetAlarm setAlarm) {
        Date timestamp = setAlarm.getTimestamp();
        boolean periodical = setAlarm.isPeriodical();
        TimerQueue.getQueue().addTask(timestamp, periodical);
    }
    
}
