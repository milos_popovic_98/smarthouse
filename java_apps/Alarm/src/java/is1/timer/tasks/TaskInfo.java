package is1.timer.tasks;

import java.util.Date;

public class TaskInfo {
    private Date timestamp;
    private boolean periodical;

    public TaskInfo(Date timestamp, boolean periodical) {
        this.timestamp = timestamp;
        this.periodical = periodical;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isPeriodical() {
        return periodical;
    }

    public void setPeriodical(boolean periodical) {
        this.periodical = periodical;
    }
}
