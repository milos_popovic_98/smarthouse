package is1.timer.tasks;


import java.util.Comparator;

public class TaskComparator implements Comparator<TaskInfo> {

    @Override
    public int compare(TaskInfo o1, TaskInfo o2) {
        return o1.getTimestamp().compareTo(o2.getTimestamp());
    }
}
