package is1.timer.tasks;

import is1.timer.TimerQueue;
import java.io.File;
import java.io.IOException;

import java.util.TimerTask;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AlarmTask extends Thread {

    private static String alarm = "C:\\Users\\popov\\Desktop\\Informacioni sistemi\\is_projekat\\java_aplikacije\\Alarm3\\out\\artifacts\\JavaFXApp\\JavaFXApp.jar";

    private TaskInfo taskInfo;
    private Semaphore semaphore;

    public AlarmTask(TaskInfo taskInfo) {
        this.taskInfo = taskInfo;
        this.semaphore = new Semaphore(0);
    }

    @Override
    public void run() {
        try {
            do {
                long period = taskInfo.getTimestamp().getTime() - System.currentTimeMillis();
                System.out.println(period + "");
                boolean woken = semaphore.tryAcquire(period, TimeUnit.MILLISECONDS);
                if (!woken) {
                    String command = "cmd.exe /c  java -jar " + '"' + alarm + '"';
                    System.out.println(command);
                    ProcessBuilder processBuilder = new ProcessBuilder(command.split(" "));
                    processBuilder.directory(new File("C:/Users/popov/"));
                    processBuilder.start();
                    TimerQueue.getQueue().finishTask();
                }
                taskInfo = TimerQueue.getQueue().getNextTask();
            } while (taskInfo != null);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(AlarmTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Semaphore getSemaphore() {
        return semaphore;
    }

}
