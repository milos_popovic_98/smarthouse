package is1.timer;

import is1.timer.tasks.AlarmTask;
import is1.timer.tasks.TaskComparator;
import is1.timer.tasks.TaskInfo;

import java.util.Date;
import java.util.PriorityQueue;

public class TimerQueue {
    private static long DAY_IN_MILLIS = 24 * 60 * 60 * 1000;
    private static TimerQueue timerQueue;
    private PriorityQueue<TaskInfo> tasks;
    private AlarmTask alarmTask;

    private TimerQueue() {
        tasks = new PriorityQueue<>(new TaskComparator());
    }

    public static TimerQueue getQueue() {
        if (timerQueue == null) {
            timerQueue = new TimerQueue();
        }
        return timerQueue;
    }

    public void addTask(Date timestamp, boolean periodical) {
        System.out.println("add task ");
        TaskInfo taskInfo = new TaskInfo(timestamp, periodical);
        if (alarmTask == null) {
            System.out.println("Create alarm task");
            alarmTask = new AlarmTask(taskInfo);
            alarmTask.start();
            tasks.add(taskInfo);
        } else {
            tasks.add(taskInfo);
            if (tasks.peek() == taskInfo) {
                alarmTask.getSemaphore().release();
            }
        }

    }

    public TaskInfo getNextTask() {
        if (tasks.peek() == null) {
            alarmTask = null;
        }
        return tasks.peek();
    }

    public void finishTask() {
        TaskInfo taskInfo = tasks.poll();
        System.out.println("Finish task called");
        if (taskInfo.isPeriodical()) {
            taskInfo.setTimestamp(new Date(taskInfo.getTimestamp().getTime() + DAY_IN_MILLIS));
            tasks.add(taskInfo);
        }
    }
}
