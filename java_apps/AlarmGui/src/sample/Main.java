package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main extends Application {

    private List<String> musicFiles;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Alarm");

        musicFiles = new ArrayList<>();
        musicFiles.add("C:\\Users\\popov\\Desktop\\Informacioni sistemi\\is_projekat\\java_aplikacije\\Alarm3\\alarm1.mp3");
        musicFiles.add("C:\\Users\\popov\\Desktop\\Informacioni sistemi\\is_projekat\\java_aplikacije\\Alarm3\\alarm2.mp3");
        musicFiles.add("C:\\Users\\popov\\Desktop\\Informacioni sistemi\\is_projekat\\java_aplikacije\\Alarm3\\alarm3.mp3");
        musicFiles.add("C:\\Users\\popov\\Desktop\\Informacioni sistemi\\is_projekat\\java_aplikacije\\Alarm3\\alarm4.mp3");
        musicFiles.add("C:\\Users\\popov\\Desktop\\Informacioni sistemi\\is_projekat\\java_aplikacije\\Alarm3\\alarm5.mp3");

        File file = new File("C:\\Users\\popov\\Desktop\\Informacioni sistemi\\is_projekat\\java_aplikacije\\Alarm3\\alarm.txt");
        Scanner reader = new Scanner(file);
        String data = reader.next();
        int index = Integer.parseInt(data);

        Media sound = new Media(new File(musicFiles.get(index)).toURI().toString());
        MediaPlayer player = new MediaPlayer(sound);
        player.play();
        StackPane pane = new StackPane();
        pane.setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));

        Button button = new Button("Stop");
        button.setPrefHeight(50);
        button.setPrefWidth(100);
        button.setOnAction(e -> {
            player.stop();
        });
        pane.getChildren().add(button);

        Scene scene = new Scene(pane, 300, 275);

        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
