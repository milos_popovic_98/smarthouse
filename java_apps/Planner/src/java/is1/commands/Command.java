/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is1.commands;

import is1.visitors.Visitor;
import java.io.Serializable;

/**
 *
 * @author popov
 */
public interface Command extends Serializable {
    public int getSenderId();
    public void setSenderId(int id);
    public int getReceiverId();
    public void execute(Visitor visitor);
    public void setUserid(int id);
    public int getUserId();
}