/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is1.commands;

import is1.entities.Location;
import is1.visitors.Visitor;

/**
 *
 * @author popov
 */
public class CalculateDistance implements Command{

    private static int PLANNER = 2;
    private int senderId;
    private int userId;
    private Location start;
    private Location target;
    private Long response;
    
    @Override
    public int getSenderId() {
        return senderId;
    }

    @Override
    public void setSenderId(int id) {
        senderId = id;
    }

    @Override
    public int getReceiverId() {
        return PLANNER;
    }

    @Override
    public void execute(Visitor visitor) {
       visitor.visit(this);
    }

    @Override
    public void setUserid(int id) {
        userId = id;
    }

    @Override
    public int getUserId() {
       return userId;
    }

    public Location getStart() {
        return start;
    }

    public void setStart(Location start) {
        this.start = start;
    }

    public Location getTarget() {
        return target;
    }

    public void setTarget(Location target) {
        this.target = target;
    }

    public Long getResponse() {
        return response;
    }

    public void setResponse(Long response) {
        this.response = response;
    }
    
}
