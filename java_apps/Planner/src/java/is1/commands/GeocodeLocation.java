/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is1.commands;

import is1.entities.Location;
import is1.visitors.Visitor;

/**
 *
 * @author popov
 */
public class GeocodeLocation implements Command{

    private static int PLANNER = 2;
    private int senderId;
    private int userId;
    private Location home;
    private String locationName;
    private Location response;
    
    @Override
    public int getSenderId() {
        return senderId;
    }

    @Override
    public void setSenderId(int id) {
        senderId = id;
    }

    @Override
    public int getReceiverId() {
        return PLANNER;
    }

    @Override
    public void execute(Visitor visitor) {
       visitor.visit(this);
    }

    @Override
    public void setUserid(int id) {
        userId = id;
    }

    @Override
    public int getUserId() {
       return userId;
    }

    public Location getHome() {
        return home;
    }

    public void setHome(Location home) {
        this.home = home;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Location getResponse() {
        return response;
    }

    public void setResponse(Location response) {
        this.response = response;
    }
    
    
}
