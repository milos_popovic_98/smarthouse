package is1.travel;

import is1.entities.Location;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;

public class TravelTimeCalculator {
    private static TravelTimeCalculator calculator;

    private TravelTimeCalculator() {

    }

    public static TravelTimeCalculator getCalculator() {
        if (calculator == null) {
            calculator = new TravelTimeCalculator();
        }
        return calculator;
    }

    public Long getTravelTime(Location start, Location target) throws IOException {
        System.out.println(start.getLocationLat() + " " + start.getLocationLon());
        System.out.println(target.getLocationLat() + " " + target.getLocationLon());
        
        String command = "curl -X GET \"https://api.tomtom.com/routing/1/calculateRoute/" +
                start.getLocationLat() + "%2C" +
                start.getLocationLon() + "%3A" +
                target.getLocationLat() + "%2C" +
                target.getLocationLon() +
                "/json?avoid=unpavedRoads&key=NEqs6LSejxwL9oZhexTiZsgRLYZrc38t\" -H \"accept: */*";
        ProcessBuilder processBuilder = new ProcessBuilder(command.split(" "));
        processBuilder.directory(new File("C:/Users/popov/"));
        Process process = processBuilder.start();
        InputStream in = process.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
        JSONTokener tokener = new JSONTokener(bufferedReader);
        JSONObject json = new JSONObject(tokener);
        JSONArray routes = json.getJSONArray("routes");
        if (!routes.isEmpty()) {
            return routes.getJSONObject(0).getJSONObject("summary").getLong("travelTimeInSeconds");
        } else {
            return null;
        }
    }
}
