package is1;



public class PlannerService {
    private static PlannerService service;

    private PlannerService() {}

    public static PlannerService getPlannerService() {
        if (service == null) {
            service = new PlannerService();
        }
        return service;
    }
/*
    public List<Event> getEvents(int idUser) {
        EntityManager entityManager = EM.getInstance().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            Collection<Event> events = entityManager.find(User.class, idUser).getEventCollection();
            List<Event> filteredEvents = new ArrayList<>();
            for(Event e : events) {
                if (e.getEndTime().getTime() < System.currentTimeMillis()) {
                    entityManager.remove(e);
                } else {
                    filteredEvents.add(e);
                }
            }
            transaction.commit();
            return filteredEvents;
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
    }

    public boolean updateEvent(Event event) {
        EntityManager entityManager = EM.getInstance().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            if (!checkTimeConstraints(entityManager, event)) {
                return false;
            }
            transaction.commit();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
        return false;
    }

    public boolean addEvent(Event event) {
        long startTime = event.getStartTime().getTime();
        if (startTime < System.currentTimeMillis()) {
            return false;
        }
        EntityManager entityManager = EM.getInstance().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            if (!checkTimeConstraints(entityManager, event)) {
                return false;
            }
            entityManager.persist(event);
            transaction.commit();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
        return false;
    }

    public void removeEvent(Event event) {
        EntityManager entityManager = EM.getInstance().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.remove(event);
            transaction.commit();
            //return true;
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
    }

    protected boolean checkTimeConstraints(EntityManager entityManager, Event event) throws IOException {
        TypedQuery<Event> query = entityManager.createQuery("SELECT e FROM Event e WHERE e.endTime < :time ORDER BY e.endTime DESC", Event.class)
                .setParameter("time", event.getEndTime())
                .setMaxResults(1);
        List<Event> temp = query.getResultList();
        if (!temp.isEmpty() && !checkEventsOverlapping(temp.get(0), event)) {
            return false;
        }
        TypedQuery<Event> query2 = entityManager.createQuery("SELECT e FROM Event e WHERE e.startTime >= :time ORDER BY e.startTime", Event.class)
                .setParameter("time", event.getEndTime())
                .setMaxResults(1);
        temp = query2.getResultList();
        if (!temp.isEmpty() && !checkEventsOverlapping(event, temp.get(0))) {
            return false;
        }
        return true;
    }

    protected boolean checkEventsOverlapping(Event event1, Event event2) throws IOException {
        Location start = event1.getEventlocation().getIdLoc();
        Location target = event2.getEventlocation().getIdLoc();
        long travelTimeInSeconds = TravelTimeCalculator.getCalculator().getTravelTime(start, target);
        if (event1.getEndTime().getTime() + travelTimeInSeconds > event2.getStartTime().getTime()) {
            return false;
        } else {
            return true;
        }
    }*/
}
