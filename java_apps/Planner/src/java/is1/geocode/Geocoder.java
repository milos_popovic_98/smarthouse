package is1.geocode;

import is1.entities.Location;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;


import java.io.*;

public class Geocoder {
    private static Geocoder geocoder;

    private Geocoder() {

    }

    public static Geocoder getGeocoder() {
        if (geocoder == null) {
            geocoder = new Geocoder();
        }
        return geocoder;
    }

    public Location geocodeLocation(String locationName, Location bias) throws IOException {
        String encodedLocationName = locationName.replaceAll(" ", "%20");
        String command = "curl -X GET \"https://api.tomtom.com/search/2/geocode/"
                + encodedLocationName +
                ".json?lat=" + bias.getLocationLat() +
                "&lon=" + bias.getLocationLon() +
                "&key=NEqs6LSejxwL9oZhexTiZsgRLYZrc38t\" -H \"accept: */*";
        ProcessBuilder processBuilder = new ProcessBuilder(command.split(" "));
        processBuilder.directory(new File("C:/Users/popov/"));
        Process process = processBuilder.start();
        InputStream in = process.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
        JSONTokener tokener = new JSONTokener(bufferedReader);
        JSONObject json = new JSONObject(tokener);
        JSONArray results = json.getJSONArray("results");
        if (!results.isEmpty()) {
            JSONObject position = results.getJSONObject(0).getJSONObject("position");
            return new Location(position.getBigDecimal("lon"), position.getBigDecimal("lat"));
        } else {
            return null;
        }
    }
}
