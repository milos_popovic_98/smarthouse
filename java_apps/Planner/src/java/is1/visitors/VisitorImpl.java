/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is1.visitors;

import is1.commands.CalculateDistance;
import is1.commands.GeocodeLocation;
import is1.commands.PlaySong;
import is1.commands.SetAlarm;
import is1.entities.Location;
import is1.geocode.Geocoder;
import is1.travel.TravelTimeCalculator;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author popov
 */
public class VisitorImpl implements Visitor{

    @Override
    public void visit(PlaySong playSong) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(GeocodeLocation geocodeLocation) {
        try {
            String locationName = geocodeLocation.getLocationName();
            Location bias = geocodeLocation.getHome();
            geocodeLocation.setResponse(Geocoder.getGeocoder().geocodeLocation(locationName, bias));
        } catch (IOException ex) {
            Logger.getLogger(VisitorImpl.class.getName()).log(Level.SEVERE, null, ex);
            geocodeLocation.setResponse(null);
        }
    }

    @Override
    public void visit(CalculateDistance calculateDistance) {
        try {
            Location start = calculateDistance.getStart();
            Location target = calculateDistance.getTarget();
            calculateDistance.setResponse(TravelTimeCalculator.getCalculator().getTravelTime(start, target));
        } catch (IOException ex) {
            Logger.getLogger(VisitorImpl.class.getName()).log(Level.SEVERE, null, ex);
            calculateDistance.setResponse(null);
        }
    }

    @Override
    public void visit(SetAlarm setAlarm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
