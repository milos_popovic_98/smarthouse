package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.json.JSONArray;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Main extends Application {

    private Scene registrationScene;
    private Scene loginScene;
    private Stage primaryStage;
    private Scene mainScene;
    private Scene searchScene;
    private Scene playlistScene;
    private Scene eventListScene;
    private Scene addEventScene;
    private Scene deleteEventScene;
    private Scene updateEventScene;
    private Scene calculateDistanceScene;
    private Scene configureMelody;
    private Scene newAlarmScene;

    private String username;
    private String password;

    private TableView<Song> tablePlaylist;
    private TableView<Event> tableEventList;
    private ArrayList<Event> eventsList = new ArrayList<>();
    private ArrayList<SelectedEvent> idEvents = new ArrayList<>();
    private ComboBox comboBoxDeleteEvent;
    private ComboBox comboBoxUpdateEvent;
    private ComboBox comboBoxMelody;
    private List<MusicFile> musicFiles = new ArrayList<>();

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("SMART HOUSE");
        this.primaryStage = primaryStage;

        musicFiles.add(new MusicFile(0,"alarm1"));
        musicFiles.add(new MusicFile(1,"alarm2"));
        musicFiles.add(new MusicFile(2, "alarm3"));
        musicFiles.add(new MusicFile(3, "alarm4"));
        musicFiles.add(new MusicFile(4, "alarm5"));

        GridPane loginGridPane = createFormPane();
        addUILoginControls(loginGridPane);
        loginScene = new Scene(loginGridPane, 800, 500);

        GridPane gridPane = createFormPane();
        addUIControls(gridPane);
        registrationScene = new Scene(gridPane, 800, 500);

        GridPane mainGridPane = createMainPane();
        addUIMainControls(mainGridPane);
        mainScene = new Scene(mainGridPane, 800, 500);

        GridPane searchSongGridPane = createFormPane();
        searchSongGridPane.setBackground(new Background(new BackgroundFill(Color.LIGHTGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
        addUISearchSongControls(searchSongGridPane);
        searchScene = new Scene(searchSongGridPane, 800, 500);

        VBox playlistPane = createPlaylist();
        playlistPane.setBackground(new Background(new BackgroundFill(Color.LIGHTGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
        addUIListControls(playlistPane);
        playlistScene = new Scene(playlistPane, 800, 500);

        VBox eventListPane = createEventList();
        eventListPane.setBackground(new Background(new BackgroundFill(Color.LIGHTSEAGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
        addUIListControls(eventListPane);
        eventListScene = new Scene(eventListPane, 800, 500);

        GridPane addEventPane = createFormPane();
        addEventPane.setBackground(new Background(new BackgroundFill(Color.LIGHTSEAGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
        addUIAddEventControls(addEventPane);
        addEventScene = new Scene(addEventPane, 800, 500);

        GridPane deleteEventPane = createFormPane();
        deleteEventPane.setBackground(new Background(new BackgroundFill(Color.LIGHTSEAGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
        addUIDeleteEventControls(deleteEventPane);
        deleteEventScene = new Scene(deleteEventPane, 800, 500);

        GridPane updateEventPane = createFormPane();
        updateEventPane.setBackground(new Background(new BackgroundFill(Color.LIGHTSEAGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
        addUIUpdateEventControls(updateEventPane);
        updateEventScene = new Scene(updateEventPane, 800, 500);

        GridPane calculateDistancePane = createFormPane();
        calculateDistancePane.setBackground(new Background(new BackgroundFill(Color.LIGHTSEAGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
        addUICalculateControls(calculateDistancePane);
        calculateDistanceScene = new Scene(calculateDistancePane, 800, 500);

        GridPane configureMelodyPane = createFormPane();
        configureMelodyPane.setBackground(new Background(new BackgroundFill(Color.ORANGE, CornerRadii.EMPTY, Insets.EMPTY)));
        addUIConfigureControls(configureMelodyPane);
        configureMelody = new Scene(configureMelodyPane, 800, 500);

        GridPane newAlarmPane = createFormPane();
        newAlarmPane.setBackground(new Background(new BackgroundFill(Color.ORANGE, CornerRadii.EMPTY, Insets.EMPTY)));
        addUISetAlarmControls(newAlarmPane);
        newAlarmScene = new Scene(newAlarmPane, 800, 500);

        primaryStage.setScene(loginScene);
        //primaryStage.setScene(mainScene);
        primaryStage.show();
    }

    protected void addUISetAlarmControls(GridPane newAlarmPane) {
        Label name = new Label("Date and time: ");
        name.setAlignment(Pos.BASELINE_RIGHT);
        name.setPrefWidth(100);
        newAlarmPane.add(name, 0, 1);
        TextField dateField = new TextField();
        dateField.setPrefHeight(40);
        newAlarmPane.add(dateField, 1, 1);

        CheckBox alarm = new CheckBox("Repeat");
        alarm.setPrefHeight(40);
        newAlarmPane.add(alarm, 1, 2);

        Button confirm = new Button("Confirm");
        confirm.setPrefHeight(40);
        confirm.setDefaultButton(true);
        confirm.setPrefWidth(150);
        newAlarmPane.add(confirm, 1, 3);
        GridPane.setHalignment(confirm, HPos.CENTER);
        GridPane.setMargin(confirm, new Insets(20, 0, 0, 0));

        confirm.setOnAction(e -> {
            String time = dateField.getText();
            boolean flag = alarm.isSelected();
            String response = sendNewAlarmRequest(time, flag);
            if ("OK".equals(response)) {
                showAlert(Alert.AlertType.INFORMATION, newAlarmPane.getScene().getWindow(),
                        "Info", "Alarm is set");
            }
        });

        Button mainPage = new Button("Main menu");
        mainPage.setPrefHeight(40);
        mainPage.setDefaultButton(true);
        mainPage.setPrefWidth(150);
        newAlarmPane.add(mainPage, 1, 4);
        GridPane.setHalignment(mainPage, HPos.CENTER);
        GridPane.setMargin(mainPage, new Insets(0, 0, 0, 0));
        mainPage.setOnAction(e -> {
            primaryStage.setScene(mainScene);
        });

    }

    private String sendNewAlarmRequest(String time, boolean flag) {
        try {
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.UTF_8));
            String encodedTime = time.replaceAll(" ", "%20");
            String queryParams = "?time=" + encodedTime + "&periodical=" + flag;
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .uri(new URI("http://localhost:8080/UserService/api/alarm1" + queryParams))
                    .POST(HttpRequest.BodyPublishers.noBody())
                    .header("Authorization", "Basic " + new String(encodedAuth))
                    .build();
            HttpResponse<String> response = HttpClient.newHttpClient().send(httpRequest, HttpResponse.BodyHandlers.ofString());
            return response.body();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void addUIConfigureControls(GridPane configureMelodyPane) {
        Label name = new Label("Select melody: ");
        name.setAlignment(Pos.BASELINE_RIGHT);
        name.setPrefWidth(100);
        configureMelodyPane.add(name, 0, 1);
        comboBoxMelody = new ComboBox();
        comboBoxMelody.setPrefWidth(200);
        comboBoxMelody.setItems(FXCollections.observableArrayList(musicFiles));
        configureMelodyPane.add(comboBoxMelody, 1, 1);

        Button change = new Button("Change melody");
        change.setPrefHeight(40);
        change.setDefaultButton(true);
        change.setPrefWidth(100);
        configureMelodyPane.add(change, 1, 2);
        GridPane.setHalignment(change, HPos.CENTER);
        GridPane.setMargin(change, new Insets(0, 0, 0, 0));
        change.setOnAction(e -> {
            MusicFile file = (MusicFile)comboBoxMelody.getValue();
            if (file != null) {
                String fileName = "C:\\Users\\popov\\Desktop\\Informacioni sistemi\\is_projekat\\java_aplikacije\\Alarm3\\alarm.txt";
                try {
                    BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                    writer.write("" + file.getId());
                    writer.close();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });

        Button mainPage = new Button("Main menu");
        mainPage.setPrefHeight(40);
        mainPage.setDefaultButton(true);
        mainPage.setPrefWidth(100);
        configureMelodyPane.add(mainPage, 1, 3);
        GridPane.setHalignment(mainPage, HPos.CENTER);
        GridPane.setMargin(mainPage, new Insets(0, 0, 0, 0));
        mainPage.setOnAction(e -> {
            primaryStage.setScene(mainScene);
        });
    }

    protected void addUICalculateControls(GridPane calculateDistancePane) {
        Label startLabel = new Label("Start: ");
        calculateDistancePane.add(startLabel, 0, 1);
        TextField startField = new TextField();
        startField.setText("Home");
        startField.setPrefHeight(40);
        calculateDistancePane.add(startField, 1, 1);


        Label dstLabel = new Label("Destination: ");
        calculateDistancePane.add(dstLabel, 0, 2);
        TextField dstField = new TextField();
        dstField.setPrefHeight(40);
        calculateDistancePane.add(dstField, 1, 2);

        Button calculateButton = new Button("Calculate travel time");
        calculateButton.setPrefHeight(40);
        calculateButton.setDefaultButton(true);
        calculateButton.setPrefWidth(150);
        calculateDistancePane.add(calculateButton, 1, 3);
        GridPane.setHalignment(calculateButton, HPos.CENTER);
        GridPane.setMargin(calculateButton, new Insets(20, 0, 0, 0));

        calculateButton.setOnAction(e -> {
            String start = startField.getText();
            String dst = dstField.getText();
            String response = sendCalculateRequest(start, dst);
            showAlert(Alert.AlertType.INFORMATION, calculateDistancePane.getScene().getWindow(),
                    "Travel time", response);
        });

        Button mainPage = new Button("Main menu");
        mainPage.setPrefHeight(40);
        mainPage.setDefaultButton(true);
        mainPage.setPrefWidth(150);
        calculateDistancePane.add(mainPage, 1, 4);
        GridPane.setHalignment(mainPage, HPos.CENTER);
        GridPane.setMargin(mainPage, new Insets(0, 0, 0, 0));
        mainPage.setOnAction(e -> {
            primaryStage.setScene(mainScene);
        });

    }

    protected String sendCalculateRequest(String start, String dst) {
        try {
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.UTF_8));
            String encodedDst = dst.replaceAll(" ", "%20");
            String encodedStart = start.replaceAll(" ", "%20");
            String queryParams;
            if ("Home".equals(start)) {
                queryParams = "/current?target=" + encodedDst;
            } else {
                queryParams = "?start=" + encodedStart + "&target=" + encodedDst;
            }
            HttpRequest httpRequest = null;
            httpRequest = HttpRequest.newBuilder()
                    .uri(new URI("http://localhost:8080/UserService/api/calculate" + queryParams))
                    .GET()
                    .header("Authorization", "Basic " + new String(encodedAuth))
                    .build();
            HttpResponse<String> response = HttpClient.newHttpClient().send(httpRequest, HttpResponse.BodyHandlers.ofString());
            Long travelTimeInSeconds = Long.parseLong(response.body());
            System.out.println(travelTimeInSeconds);
            long seconds = travelTimeInSeconds % 60;
            long minutes = travelTimeInSeconds / 60 % 60;
            long hours = travelTimeInSeconds / 3600;
            String ret = (hours > 0) ? hours + ":" : "";
            String min = (minutes > 9) ? minutes + "" : "0" + minutes;
            String sec = (seconds > 9) ? seconds + "" : "0" + seconds;
            ret = ret + min + ":" + sec;
            return ret;
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void addUIUpdateEventControls(GridPane updateEventPane) {
        Label name = new Label("Select event: ");
        name.setAlignment(Pos.BASELINE_RIGHT);
        name.setPrefWidth(100);
        updateEventPane.add(name, 0, 1);
        comboBoxUpdateEvent = new ComboBox();
        comboBoxUpdateEvent.setPrefWidth(200);
        updateEventPane.add(comboBoxUpdateEvent, 1, 1);

        Label begin = new Label("Begin: ");
        updateEventPane.add(begin, 0, 2);
        TextField startField = new TextField();
        startField.setPrefHeight(40);
        updateEventPane.add(startField, 1, 2);

        Label end = new Label("End: ");
        updateEventPane.add(end, 0, 3);
        TextField endField = new TextField();
        endField.setPrefHeight(40);
        updateEventPane.add(endField, 1, 3);

        Label destination = new Label("Location: ");
        updateEventPane.add(destination, 0, 4);
        TextField dstField = new TextField();
        dstField.setPrefHeight(40);
        updateEventPane.add(dstField, 1, 4);

        Button mainPage = new Button("Main menu");
        mainPage.setPrefHeight(40);
        mainPage.setDefaultButton(true);
        mainPage.setPrefWidth(100);
        updateEventPane.add(mainPage, 1, 6);
        GridPane.setHalignment(mainPage, HPos.CENTER);
        GridPane.setMargin(mainPage, new Insets(0, 0, 0, 0));
        mainPage.setOnAction(e -> {
            primaryStage.setScene(mainScene);
        });

        Button updateButton = new Button("Update");
        updateButton.setPrefHeight(40);
        updateButton.setDefaultButton(true);
        updateButton.setPrefWidth(100);
        updateEventPane.add(updateButton, 1, 5);
        GridPane.setHalignment(updateButton, HPos.CENTER);
        GridPane.setMargin(updateButton, new Insets(0, 0, 0, 0));
        updateButton.setOnAction(e -> {
            if (comboBoxUpdateEvent.getValue() == null) {
                showAlert(Alert.AlertType.ERROR, updateButton.getScene().getWindow(),
                        "Form Error!", "Please select event");
                return;
            }
            String response = sendUpdateEventRequest(dstField.getText(), startField.getText(), endField.getText());
            showAlert(Alert.AlertType.INFORMATION, updateButton.getScene().getWindow(),
                    "Status", response);
            sendEventsRequest();
            comboBoxUpdateEvent.setItems(FXCollections.observableArrayList(idEvents));
            return;
        });
        comboBoxUpdateEvent.setOnAction(e -> {
            SelectedEvent event = (SelectedEvent) comboBoxUpdateEvent.getValue();
            if (event != null) {
                for (Event event1 : eventsList) {
                    if (event1.getTitle().equals(event.getName())) {
                        startField.setText(event1.getStartTime());
                        endField.setText(event1.getEndTime());
                        break;
                    }
                }
            }
        });
    }

    protected String sendUpdateEventRequest(String dstParam, String startParam, String endParam) {
        try {
            SelectedEvent event = (SelectedEvent) comboBoxUpdateEvent.getValue();
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.UTF_8));
            startParam = reverseDate(startParam);
            endParam = reverseDate(endParam);
            String encodedDst = dstParam.replaceAll(" ", "%20");
            String encodedStart = startParam.replaceAll(" ", "%20");
            String encodedEnd = endParam.replaceAll(" ", "%20");
            String queryParams = "?start1=" + encodedStart + "&end1=" + encodedEnd + "&destination=" + encodedDst;
            HttpRequest httpRequest = null;
            httpRequest = HttpRequest.newBuilder()
                    .uri(new URI("http://localhost:8080/UserService/api/events/update/" + event.getId() + queryParams))
                    .POST(HttpRequest.BodyPublishers.noBody())
                    .header("Authorization", "Basic " + new String(encodedAuth))
                    .build();
            HttpResponse<String> response = HttpClient.newHttpClient().send(httpRequest, HttpResponse.BodyHandlers.ofString());
            System.out.println(response.body());
            return response.body();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String reverseDate(String startParam) {
        StringTokenizer st = new StringTokenizer(startParam, " ");
        String date = st.nextToken();
        String time = st.nextToken();
        st = new StringTokenizer(date, "-");
        String day = st.nextToken();
        String month = st.nextToken();
        String year = st.nextToken();
        return year + "-" + month + "-" + day + " " + time;
    }

    protected void addUIDeleteEventControls(GridPane deleteEventPane) {
        Label name = new Label("Select event: ");
        name.setAlignment(Pos.BASELINE_RIGHT);
        name.setPrefWidth(100);
        deleteEventPane.add(name, 0, 1);
        comboBoxDeleteEvent = new ComboBox();
        comboBoxDeleteEvent.setPrefWidth(200);
        deleteEventPane.add(comboBoxDeleteEvent, 1, 1);

        Button mainPage = new Button("Main menu");
        mainPage.setPrefHeight(40);
        mainPage.setDefaultButton(true);
        mainPage.setPrefWidth(100);
        deleteEventPane.add(mainPage, 1, 3);
        GridPane.setHalignment(mainPage, HPos.CENTER);
        GridPane.setMargin(mainPage, new Insets(0, 0, 0, 0));
        mainPage.setOnAction(e -> {
            primaryStage.setScene(mainScene);
        });

        Button deleteButton = new Button("Delete");
        deleteButton.setPrefHeight(40);
        deleteButton.setDefaultButton(true);
        deleteButton.setPrefWidth(100);
        deleteEventPane.add(deleteButton, 1, 2);
        GridPane.setHalignment(deleteButton, HPos.CENTER);
        GridPane.setMargin(deleteButton, new Insets(0, 0, 0, 0));
        deleteButton.setOnAction(e -> {
            if (comboBoxDeleteEvent.getValue() == null) {
                showAlert(Alert.AlertType.ERROR, deleteEventPane.getScene().getWindow(),
                        "Form Error!", "Please select event");
                return;
            }
            sendDeleteEventRequest();
            showAlert(Alert.AlertType.INFORMATION, deleteEventPane.getScene().getWindow(),
                    "Status", "Event is deleted");
            sendEventsRequest();
            comboBoxDeleteEvent.setItems(FXCollections.observableArrayList(idEvents));
            return;
        });

    }

    protected void sendDeleteEventRequest() {
        try {
            SelectedEvent event = (SelectedEvent) comboBoxDeleteEvent.getValue();
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.UTF_8));
            HttpRequest httpRequest = null;
            httpRequest = HttpRequest.newBuilder()
                    .uri(new URI("http://localhost:8080/UserService/api/events/" + event.getId()))
                    .DELETE()
                    .header("Authorization", "Basic " + new String(encodedAuth))
                    .build();
            HttpResponse<String> response = HttpClient.newHttpClient().send(httpRequest, HttpResponse.BodyHandlers.ofString());
            System.out.println(response.body());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    protected void addUIAddEventControls(GridPane addEventPane) {
        Label name = new Label("Name: ");
        addEventPane.add(name, 0, 1);
        TextField nameField = new TextField();
        nameField.setPrefHeight(40);
        addEventPane.add(nameField, 1, 1);

        Label begin = new Label("Begin: ");
        addEventPane.add(begin, 0, 2);
        TextField startField = new TextField();
        startField.setPrefHeight(40);
        addEventPane.add(startField, 1, 2);

        Label end = new Label("End: ");
        addEventPane.add(end, 0, 3);
        TextField endField = new TextField();
        endField.setPrefHeight(40);
        addEventPane.add(endField, 1, 3);

        Label destination = new Label("Location: ");
        addEventPane.add(destination, 0, 4);
        TextField dstField = new TextField();
        dstField.setPrefHeight(40);
        addEventPane.add(dstField, 1, 4);

        CheckBox alarm = new CheckBox("Set alarm");
        addEventPane.add(alarm, 1, 5);

        Button mainPage = new Button("Main menu");
        mainPage.setPrefHeight(40);
        mainPage.setDefaultButton(true);
        mainPage.setPrefWidth(100);
        addEventPane.add(mainPage, 1, 7);
        GridPane.setHalignment(mainPage, HPos.CENTER);
        GridPane.setMargin(mainPage, new Insets(0, 0, 0, 0));
        mainPage.setOnAction(e -> {
            primaryStage.setScene(mainScene);
        });

        Button addButton = new Button("Add new event");
        addButton.setPrefHeight(40);
        addButton.setDefaultButton(true);
        addButton.setPrefWidth(100);
        addEventPane.add(addButton, 1, 6);
        GridPane.setHalignment(addButton, HPos.CENTER);
        GridPane.setMargin(addButton, new Insets(0, 0, 0, 0));
        addButton.setOnAction(e -> {
            String nameParam = nameField.getText();
            String dstParam = dstField.getText();
            String startParam = startField.getText();
            String endParam = endField.getText();
            boolean flag = alarm.isSelected();
            if ("".equals(nameParam) || "".equals(startParam) || "".equals(endParam)) {
                showAlert(Alert.AlertType.ERROR, addEventPane.getScene().getWindow(),
                        "Form Error!", "Please fill the form");
                return;
            }
            String response = sendAddEventRequest(nameParam, dstParam, startParam, endParam, flag);
            showAlert(Alert.AlertType.INFORMATION, addEventPane.getScene().getWindow(),
                    "Status", response);
            return;
        });
    }

    protected String sendAddEventRequest(String nameParam, String dstParam, String startParam, String endParam, boolean flag) {
        try {
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.UTF_8));
            String encodedName = nameParam.replaceAll(" ", "%20");
            String encodedDst = dstParam.replaceAll(" ", "%20");
            String encodedStart = startParam.replaceAll(" ", "%20");
            String encodedEnd = endParam.replaceAll(" ", "%20");
            String queryParams = "?start1=" + encodedStart + "&end1=" + encodedEnd +
                    "&destination=" + encodedDst + "&name=" + encodedName + "&reminder=" + flag;
            HttpRequest httpRequest = null;
            httpRequest = HttpRequest.newBuilder()
                    .uri(new URI("http://localhost:8080/UserService/api/events/add" + queryParams))
                    .POST(HttpRequest.BodyPublishers.noBody())
                    .header("Authorization", "Basic " + new String(encodedAuth))
                    .build();
            HttpResponse<String> response = HttpClient.newHttpClient().send(httpRequest, HttpResponse.BodyHandlers.ofString());
            System.out.println(response.body());
            return response.body();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected VBox createEventList() {
        tableEventList = new TableView<>();
        tableEventList.setPadding(new Insets(0, 0, 40, 0));
        tableEventList.setMaxWidth(700);
        Label label = new Label("Events");
        label.setFont(new Font("Arial", 20));
        label.setPadding(new Insets(0, 0, 20, 0));
        tableEventList.setEditable(true);

        TableColumn firstCol = new TableColumn("Number");
        firstCol.setMinWidth(100);
        firstCol.setCellValueFactory(
                new PropertyValueFactory<Event, Integer>("id"));

        TableColumn secondCol = new TableColumn("Name");
        secondCol.setMinWidth(200);
        secondCol.setCellValueFactory(
                new PropertyValueFactory<Event, String>("title"));

        TableColumn thirdCol = new TableColumn("Start");
        thirdCol.setMinWidth(200);
        thirdCol.setCellValueFactory(
                new PropertyValueFactory<Event, String>("startTime"));

        TableColumn fourthCol = new TableColumn("End");
        fourthCol.setMinWidth(200);
        fourthCol.setCellValueFactory(
                new PropertyValueFactory<Event, String>("endTime"));

        tableEventList.getColumns().addAll(firstCol, secondCol, thirdCol, fourthCol);
        VBox box = new VBox();
        box.setAlignment(Pos.CENTER);
        box.setPadding(new Insets(40, 40, 40, 40));
        box.getChildren().addAll(label, tableEventList);
        return box;
    }

    protected void addUIListControls(VBox box) {
        Label dummy = new Label();
        dummy.setPadding(new Insets(0, 0, 20, 0));
        Button mainPage = new Button("Main menu");
        mainPage.setPrefHeight(40);
        mainPage.setDefaultButton(true);
        mainPage.setPrefWidth(100);
        box.getChildren().addAll(dummy, mainPage);
        mainPage.setOnAction(e -> {
            primaryStage.setScene(mainScene);
        });
    }

    protected VBox createPlaylist() {
        tablePlaylist = new TableView<>();
        tablePlaylist.setPadding(new Insets(0, 0, 40, 0));
        tablePlaylist.setMaxWidth(400);
        Label label = new Label("Playlist");
        label.setFont(new Font("Arial", 20));
        label.setPadding(new Insets(0, 0, 20, 0));
        tablePlaylist.setEditable(true);

        TableColumn firstCol = new TableColumn("Number");
        firstCol.setMinWidth(100);
        firstCol.setCellValueFactory(
                new PropertyValueFactory<Song, Integer>("id"));

        TableColumn secondCol = new TableColumn("Song Name");
        secondCol.setMinWidth(300);
        secondCol.setCellValueFactory(
                new PropertyValueFactory<Song, String>("name"));

        //List<String> songNames = sendSongsRequest();
        tablePlaylist.getColumns().addAll(firstCol, secondCol);
        VBox box = new VBox();
        box.setAlignment(Pos.CENTER);
        box.setPadding(new Insets(40, 40, 40, 40));
        box.getChildren().addAll(label, tablePlaylist);
        return box;
    }

    protected List<String> sendSongsRequest() {
        try {
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.UTF_8));
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .uri(new URI("http://localhost:8080/UserService/api/music"))
                    .GET()
                    .header("Authorization", "Basic " + new String(encodedAuth))
                    .build();
            HttpResponse<String> response = HttpClient.newHttpClient().send(httpRequest, HttpResponse.BodyHandlers.ofString());
            JSONArray jsonArray = new JSONArray(response.body());
            Iterator<Object> iterator = jsonArray.iterator();
            List<String> ret = new ArrayList<>();
            while (iterator.hasNext()) {
                ret.add((String) iterator.next());
            }
            return ret;
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void addUISearchSongControls(GridPane searchSongGridPane) {
        Label song = new Label("Song: ");
        searchSongGridPane.add(song, 0, 1);
        TextField songField = new TextField();
        songField.setPrefHeight(40);
        searchSongGridPane.add(songField, 1, 1);

        Button mainPage = new Button("Main menu");
        mainPage.setPrefHeight(40);
        mainPage.setDefaultButton(true);
        mainPage.setPrefWidth(100);
        searchSongGridPane.add(mainPage, 1, 3);
        GridPane.setHalignment(mainPage, HPos.CENTER);
        GridPane.setMargin(mainPage, new Insets(0, 0, 0, 0));
        mainPage.setOnAction(e -> {
            primaryStage.setScene(mainScene);
        });

        Button searchButton = new Button("Search");
        searchButton.setPrefHeight(40);
        searchButton.setDefaultButton(true);
        searchButton.setPrefWidth(100);
        searchSongGridPane.add(searchButton, 1, 2);
        GridPane.setHalignment(searchButton, HPos.CENTER);
        GridPane.setMargin(searchButton, new Insets(0, 0, 0, 0));
        searchButton.setOnAction(e -> {
            String songName = songField.getText();
            if ("".equals(songName)) {
                showAlert(Alert.AlertType.ERROR, searchSongGridPane.getScene().getWindow(),
                        "Form Error!", "Please enter song name");
                return;
            }
            String response = sendSearchSongRequest(songName);
            if (!"OK".equals(response)) {
                showAlert(Alert.AlertType.ERROR, searchSongGridPane.getScene().getWindow(),
                        "Error!", "Sorry, We couldn't find the song with given name.");
                return;
            }
        });
    }

    protected String sendSearchSongRequest(String songName) {
        try {
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.UTF_8));
            String encodedSongName = songName.replaceAll(" ", "%20");
            String queryParams = "?song=" + encodedSongName;
            HttpRequest httpRequest = null;

            httpRequest = HttpRequest.newBuilder()
                    .uri(new URI("http://localhost:8080/UserService/api/music" + queryParams))
                    .PUT(HttpRequest.BodyPublishers.noBody())
                    .header("Authorization", "Basic " + new String(encodedAuth))
                    .build();
            HttpResponse<String> response = HttpClient.newHttpClient().send(httpRequest, HttpResponse.BodyHandlers.ofString());
            System.out.println(response.body());
            return response.body();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    protected void addUIMainControls(GridPane mainGridPane) {
        Label musicLabel = new Label("Music player");
        musicLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        mainGridPane.add(musicLabel, 0, 0);
        GridPane.setHalignment(musicLabel, HPos.CENTER);
        GridPane.setMargin(musicLabel, new Insets(0, 0, 20, 0));

        Button playSong = new Button("Search song");
        playSong.setPrefHeight(40);
        playSong.setDefaultButton(true);
        playSong.setPrefWidth(150);
        mainGridPane.add(playSong, 0, 1);
        GridPane.setHalignment(playSong, HPos.CENTER);
        GridPane.setMargin(playSong, new Insets(0, 0, 20, 0));
        playSong.setOnAction(e -> {
            primaryStage.setScene(searchScene);
        });

        Button listSongs = new Button("Show playlist");
        listSongs.setPrefHeight(40);
        listSongs.setDefaultButton(true);
        listSongs.setPrefWidth(150);
        mainGridPane.add(listSongs, 0, 2);
        GridPane.setHalignment(listSongs, HPos.CENTER);
        GridPane.setMargin(listSongs, new Insets(0, 0, 20, 0));
        listSongs.setOnAction(e -> {
            List<String> response = sendSongsRequest();
            if (response != null) {
                List<Song> songs = new ArrayList<>();
                Song.resetId();
                for (String s : response)
                    songs.add(new Song(s));
                tablePlaylist.setItems(FXCollections.observableArrayList(songs));
            }
            primaryStage.setScene(playlistScene);
        });

        Label planner = new Label("Planner");
        planner.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        mainGridPane.add(planner, 2, 0);
        GridPane.setHalignment(planner, HPos.CENTER);
        GridPane.setMargin(planner, new Insets(0, 0, 20, 0));

        Button showEvents = new Button("Show events");
        showEvents.setPrefHeight(40);
        showEvents.setDefaultButton(true);
        showEvents.setPrefWidth(150);
        mainGridPane.add(showEvents, 2, 1);
        GridPane.setHalignment(showEvents, HPos.CENTER);
        GridPane.setMargin(showEvents, new Insets(0, 0, 20, 0));
        showEvents.setOnAction(e -> {
            ArrayList<Event> response = sendEventsRequest();
            if (response != null)
                tableEventList.setItems(FXCollections.observableArrayList(response));
            primaryStage.setScene(eventListScene);
        });

        Button addEvent = new Button("Add event");
        addEvent.setPrefHeight(40);
        addEvent.setDefaultButton(true);
        addEvent.setPrefWidth(150);
        mainGridPane.add(addEvent, 2, 2);
        GridPane.setHalignment(addEvent, HPos.CENTER);
        GridPane.setMargin(addEvent, new Insets(0, 0, 20, 0));
        addEvent.setOnAction(e -> {
            primaryStage.setScene(addEventScene);
        });

        Button removeEvent = new Button("Remove event");
        removeEvent.setPrefHeight(40);
        removeEvent.setDefaultButton(true);
        removeEvent.setPrefWidth(150);
        mainGridPane.add(removeEvent, 2, 3);
        GridPane.setHalignment(removeEvent, HPos.CENTER);
        GridPane.setMargin(removeEvent, new Insets(0, 0, 20, 0));
        removeEvent.setOnAction(e -> {
            sendEventsRequest();
            comboBoxDeleteEvent.setItems(FXCollections.observableArrayList(idEvents));
            primaryStage.setScene(deleteEventScene);
        });
        Button updateEvent = new Button("Change event");
        updateEvent.setPrefHeight(40);
        updateEvent.setDefaultButton(true);
        updateEvent.setPrefWidth(150);
        mainGridPane.add(updateEvent, 2, 4);
        GridPane.setHalignment(updateEvent, HPos.CENTER);
        GridPane.setMargin(updateEvent, new Insets(0, 0, 20, 0));
        updateEvent.setOnAction(e -> {
            sendEventsRequest();
            comboBoxUpdateEvent.setItems(FXCollections.observableArrayList(idEvents));
            primaryStage.setScene(updateEventScene);
        });

        Button calculateEvent = new Button("Calculate travel time");
        calculateEvent.setPrefHeight(40);
        calculateEvent.setDefaultButton(true);
        calculateEvent.setPrefWidth(150);
        mainGridPane.add(calculateEvent, 2, 5);
        GridPane.setHalignment(calculateEvent, HPos.CENTER);
        GridPane.setMargin(calculateEvent, new Insets(0, 0, 20, 0));
        calculateEvent.setOnAction(e -> {
            primaryStage.setScene(calculateDistanceScene);
        });

        Label alarm = new Label("Alarm");
        alarm.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        mainGridPane.add(alarm, 4, 0);
        GridPane.setHalignment(alarm, HPos.CENTER);
        GridPane.setMargin(alarm, new Insets(0, 0, 20, 0));

        Button setAlarm = new Button("New Alarm");
        setAlarm.setPrefHeight(40);
        setAlarm.setDefaultButton(true);
        setAlarm.setPrefWidth(150);
        mainGridPane.add(setAlarm, 4, 1);
        GridPane.setHalignment(setAlarm, HPos.CENTER);
        GridPane.setMargin(setAlarm, new Insets(0, 0, 20, 0));
        setAlarm.setOnAction(e -> {
            primaryStage.setScene(newAlarmScene);
        });

        Button changeSound = new Button("Change sound");
        changeSound.setPrefHeight(40);
        changeSound.setDefaultButton(true);
        changeSound.setPrefWidth(150);
        mainGridPane.add(changeSound, 4, 2);
        GridPane.setHalignment(changeSound, HPos.CENTER);
        GridPane.setMargin(changeSound, new Insets(0, 0, 20, 0));
        changeSound.setOnAction(e -> {
            primaryStage.setScene(configureMelody);
        });
    }

    protected ArrayList<Event> sendEventsRequest() {
        try {
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.UTF_8));
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .uri(new URI("http://localhost:8080/UserService/api/events"))
                    .GET()
                    .header("Authorization", "Basic " + new String(encodedAuth))
                    .build();
            HttpResponse<String> response = HttpClient.newHttpClient().send(httpRequest, HttpResponse.BodyHandlers.ofString());
            System.out.println(response.body());
            JSONArray jsonArray = new JSONArray(response.body());
            Iterator<Object> iterator = jsonArray.iterator();
            List<String> temp = new ArrayList<>();
            while (iterator.hasNext()) {
                temp.add((String) iterator.next());
            }
            ArrayList<Event> events = new ArrayList<>();
            idEvents.clear();
            eventsList.clear();
            Event.resetId();
            for (int i = 0; i < temp.size(); i = i + 4) {
                idEvents.add(new SelectedEvent(Integer.parseInt(temp.get(i)), temp.get(i + 1)));
                events.add(new Event(temp.get(i + 1), temp.get(i + 2), temp.get(i + 3)));
            }
            eventsList = events;
            return events;
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected GridPane createMainPane() {
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setPadding(new Insets(0, 0, 0, 0));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setBackground(new Background(new BackgroundFill(Color.LIGHTYELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
        Separator separator = new Separator(Orientation.VERTICAL);
        gridPane.add(separator, 1, 0, 1, 6);
        separator.setPrefWidth(40);
        separator.setMaxHeight(Double.MAX_VALUE);
        Separator separator1 = new Separator(Orientation.VERTICAL);
        gridPane.add(separator1, 3, 0, 1, 6);
        separator1.setPrefWidth(40);
        separator1.setMaxHeight(Double.MAX_VALUE);
        ColumnConstraints columnOneConstraints = new ColumnConstraints(260, 260, Double.MAX_VALUE);
        columnOneConstraints.setHalignment(HPos.CENTER);
        ColumnConstraints columnSeparator = new ColumnConstraints(40, 40, Double.MAX_VALUE);
        columnSeparator.setHalignment(HPos.CENTER);
        ColumnConstraints columnTwoConstrains = new ColumnConstraints(200, 200, Double.MAX_VALUE);
        columnTwoConstrains.setHalignment(HPos.CENTER);
        ColumnConstraints columnSeparator1 = new ColumnConstraints(40, 40, Double.MAX_VALUE);
        columnSeparator1.setHalignment(HPos.CENTER);
        ColumnConstraints columnThreeConstraints = new ColumnConstraints(260, 260, Double.MAX_VALUE);
        columnThreeConstraints.setHalignment(HPos.CENTER);
        gridPane.getColumnConstraints().addAll(columnOneConstraints, columnSeparator, columnTwoConstrains, columnSeparator1, columnThreeConstraints);

        return gridPane;
    }

    protected void addUILoginControls(GridPane loginGridPane) {
        Label headerLabel = new Label("Login Form");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        loginGridPane.add(headerLabel, 0, 0, 2, 1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(20, 0, 20, 0));

        Label usernameLabel = new Label("Username");
        loginGridPane.add(usernameLabel, 0, 1);
        TextField usernameField = new TextField();
        usernameField.setPrefHeight(40);
        loginGridPane.add(usernameField, 1, 1);


        Label passwordLabel = new Label("Password : ");
        loginGridPane.add(passwordLabel, 0, 2);
        PasswordField passwordField = new PasswordField();
        passwordField.setPrefHeight(40);
        loginGridPane.add(passwordField, 1, 2);


        Button loginButton = new Button("Login");
        loginButton.setPrefHeight(40);
        loginButton.setDefaultButton(true);
        loginButton.setPrefWidth(100);
        loginGridPane.add(loginButton, 0, 4, 2, 1);
        GridPane.setHalignment(loginButton, HPos.CENTER);
        GridPane.setMargin(loginButton, new Insets(20, 0, 0, 0));

        loginButton.setOnAction(e -> {
            username = usernameField.getText();
            password = passwordField.getText();
            String response = sendLoginRequest();

            if ("OK".equals(response)) {
                primaryStage.setScene(mainScene);
            } else {
                showAlert(Alert.AlertType.ERROR, loginGridPane.getScene().getWindow(),
                        "Error!", "Wrong credentials.");
                usernameField.setText("");
                passwordField.setText("");
            }
        });

        Button registerButton = new Button("Register");
        registerButton.setPrefHeight(40);
        registerButton.setDefaultButton(true);
        registerButton.setPrefWidth(100);
        loginGridPane.add(registerButton, 0, 5, 2, 1);
        GridPane.setHalignment(registerButton, HPos.CENTER);
        GridPane.setMargin(registerButton, new Insets(0, 0, 20, 0));

        registerButton.setOnAction(e -> {
            primaryStage.setScene(registrationScene);
        });
    }


    public static void main(String[] args) {
        launch(args);
    }

    protected GridPane createFormPane() {
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setPadding(new Insets(40, 40, 40, 40));
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        ColumnConstraints columnOneConstraints = new ColumnConstraints(100, 100, Double.MAX_VALUE);
        columnOneConstraints.setHalignment(HPos.RIGHT);
        ColumnConstraints columnTwoConstrains = new ColumnConstraints(200, 200, Double.MAX_VALUE);
        columnTwoConstrains.setHgrow(Priority.ALWAYS);
        gridPane.getColumnConstraints().addAll(columnOneConstraints, columnTwoConstrains);

        return gridPane;
    }


    protected void addUIControls(GridPane gridPane) {
        Label headerLabel = new Label("Registration Form");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gridPane.add(headerLabel, 0, 0, 2, 1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(20, 0, 20, 0));

        Label usernameLabel = new Label("Username");
        gridPane.add(usernameLabel, 0, 1);
        TextField usernameField = new TextField();
        usernameField.setPrefHeight(40);
        gridPane.add(usernameField, 1, 1);

        Label passwordLabel = new Label("Password : ");
        gridPane.add(passwordLabel, 0, 2);
        PasswordField passwordField = new PasswordField();
        passwordField.setPrefHeight(40);
        gridPane.add(passwordField, 1, 2);

        Label address = new Label("Address");
        gridPane.add(address, 0, 3);
        TextField addressField = new TextField();
        addressField.setPrefHeight(40);
        gridPane.add(addressField, 1, 3);

        Button submitButton = new Button("Submit");
        submitButton.setPrefHeight(40);
        submitButton.setDefaultButton(true);
        submitButton.setPrefWidth(100);
        gridPane.add(submitButton, 0, 4, 2, 1);
        GridPane.setHalignment(submitButton, HPos.CENTER);
        GridPane.setMargin(submitButton, new Insets(20, 0, 20, 0));

        submitButton.setOnAction(new EventHandler<>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if (usernameField.getText().isEmpty()) {
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(),
                            "Form Error!", "Please enter your username");
                    return;
                }
                if (passwordField.getText().isEmpty()) {
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(),
                            "Form Error!", "Please enter your password");
                    return;
                }
                if (addressField.getText().isEmpty()) {
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(),
                            "Form Error!", "Please enter your address");
                    return;
                }

                String response = sendRegisterRequest(usernameField.getText(), passwordField.getText(), addressField.getText());
                if ("OK".equals(response)) {
                    username = usernameField.getText();
                    password = passwordField.getText();
                    showAlert(Alert.AlertType.CONFIRMATION, gridPane.getScene().getWindow(),
                            "Registration Successful!", "Welcome " + usernameField.getText());
                } else {
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(),
                            "Error!", "Username already exists");
                }
            }
        });
    }

    protected String sendLoginRequest() {
        try {
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.UTF_8));
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .uri(new URI("http://localhost:8080/UserService/api/register"))
                    .GET()
                    .header("Authorization", "Basic " + new String(encodedAuth))
                    .build();
            HttpResponse<String> response = HttpClient.newHttpClient().send(httpRequest, HttpResponse.BodyHandlers.ofString());
            System.out.println(response.body());
            return response.body();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected String sendRegisterRequest(String username, String password, String address) {
        try {
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.UTF_8));
            String encodedUsername = username.replaceAll(" ", "%20");
            String encodedPassword = password.replaceAll(" ", "%20");
            String encodedAddress = address.replaceAll(" ", "%20");
            String queryParams = "?username=" + encodedUsername + "&password=" + encodedPassword + "&location=" + encodedAddress;
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .uri(new URI("http://localhost:8080/UserService/api/register" + queryParams))
                    .POST(HttpRequest.BodyPublishers.noBody())
                    .header("Authorization", "Basic " + new String(encodedAuth))
                    .build();
            HttpResponse<String> response = HttpClient.newHttpClient().send(httpRequest, HttpResponse.BodyHandlers.ofString());
            System.out.println(response.body());
            return response.body();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
        alert.setOnCloseRequest(e -> {
            if ("Registration Successful!".equals(title)) {
                primaryStage.setScene(mainScene);
            }
        });
    }
}
