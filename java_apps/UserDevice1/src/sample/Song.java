package sample;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Song {
    private static int LAST_ID = 1;
    private final SimpleIntegerProperty id;
    private final SimpleStringProperty name;

    public static void resetId() {
        LAST_ID = 1;
    }
    public Song(String name) {
        id = new SimpleIntegerProperty(LAST_ID++);
        this.name = new SimpleStringProperty(name);
    }

    public int getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }
}
